function extend(Child, Parent) {
    var F = function() {
    }
    F.prototype = Parent.prototype;
    Child.prototype = new F();
    Child.prototype.constructor = Child;
    Child.superclass = Parent.prototype;
}

Array.prototype.remove = function() {
    var what, a = arguments, L = a.length, ax;
    while (L && this.length) {
        what = a[--L];
        while ((ax = this.indexOf(what)) !== -1) {
            this.splice(ax, 1);
        }
    }
    return this;
};

var SceneManager = (function() {

    var instance;

    function Instance() {
        this.scenes = [];

        this.add = function(scene) {
            this.scenes.push(scene);
        }

        this.remove = function(scene) {
            this.scenes.remove(scene);
        }

        this.update = function() {           
            for (var i = 0; i < this.scenes.length; i++) {
                if (this.scenes[i].enable){
                    this.scenes[i].update();
                }
            }
        }

    }

    return {
        getInstance: function() {
            if (!instance) {
                instance = new Instance();
            }

            return instance;
        }
    };
})();

function Scene() {
    var canvas = document.getElementById("canvas");
    this.ctx = canvas.getContext("2d");
    this.sceneNodes = [];
    this.quadTree = new QuadTree({x: 0, y: 0, width: this.ctx.width, height: this.ctx.height});
    this.enable = true;

    this.addSceneNode = function(node) {
        this.sceneNodes.push(node);
        node.parentScene = this;
    }

    this.removeSceneNode = function(node) {
        this.sceneNodes.remove(node);
        node.parentScene = {};
    }

    this.update = function() {
        this.quadTree.clear();
        for (var i = 0; i < this.sceneNodes.length; i++) {
            this.sceneNodes[i].clear();
            this.quadTree.insert(this.sceneNodes[i]);
        }
        this.detectCollision();

        for (var i = 0; i < this.sceneNodes.length; i++) {
            this.sceneNodes[i].draw(this.ctx);
        }
    }

    this.detectCollision = function() {
        var objects = this.sceneNodes;
        for (var x = 0, len = objects.length; x < len; x++) {
            this.quadTree.findObjects(obj = [], objects[x]);
            for (y = 0, length = obj.length; y < length; y++) {

                if (objects[x].collidableWith === obj[y].type) {
                    var leftDist = obj[y].x - (objects[x].x + objects[x].width);
                    var rightDist = objects[x].x - (obj[y].x + obj[y].width);
                    var topDist = obj[y].y - (objects[x].y + objects[x].height);
                    var bottomDist = objects[x].y - (obj[y].y + obj[y].height);

                    if (leftDist < 0 &&
                            rightDist < 0 &&
                            topDist < 0 &&
                            bottomDist < 0) {

                        var minDist = Math.max(leftDist, rightDist, topDist, bottomDist);

                        switch (minDist) {
                            case leftDist:
                                objects[x].x += minDist;
                                break;
                            case rightDist:
                                objects[x].x -= minDist;
                                break;
                            case topDist:
                                objects[x].y += minDist;
                                break;
                            case bottomDist:
                                objects[x].y -= minDist;
                                break;
                        }

                    }

                }

            }
        }
    }
}

function SceneNode(x, y, width, height) {
    this.x = x;
    this.y = y;

    this.width = width;
    this.height = height;
    this.collidableWith = "";
    this.isColliding = false;
    this.type = "";
    this.parentScene = {};

    this.move = function(x, y) {
        this.x += x;
        this.y += y;
    }

    this.setPosition = function(x, y) {
        this.x = x;
        this.y = y;
    }

    this.draw = function(ctx) {

    }

    this.clear = function() {
        this.colx = false;
        this.coly = false;
        this.isColliding = false;
    }

    this.isCollidableWith = function(object) {
        return (this.collidableWith === object.type);
    }
}



function Sprite(image, x, y, width, height) {
    Sprite.superclass.constructor.call(this, x, y, width, height);
    this.image = image;


    this.draw = function(ctx) {
        ctx.clearRect(this.x, this.y, this.width, this.height);
        ctx.drawImage(this.image, this.x, this.y, this.width, this.height);
    }
}
extend(Sprite, SceneNode);



function AnimateSprite(image, x, y, width, height) {
    AnimateSprite.superclass.constructor.call(this, x, y, width, height);
    this.image = image;
    this.frames = Math.floor(this.image.width / this.width);
    var self = this;

    var counter = 0;
    var currentFrame = 0;

    this.frameSpeed = 0.5;

    this.draw = function(ctx) {

        if (counter < self.frames - 1)
            counter += this.frameSpeed;
        else
            counter = 0;

        currentFrame = Math.floor(counter);

        ctx.clearRect(this.x, this.y, this.width, this.height);
        ctx.drawImage(
                this.image,
                currentFrame * this.width, 0,
                this.width, this.height,
                this.x, this.y,
                this.width, this.height);

    }
}
extend(AnimateSprite, SceneNode);
