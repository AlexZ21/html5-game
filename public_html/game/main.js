(function() {
    var eventHandler = EventHandler.getInstance();
    var sceneManager = SceneManager.getInstance();
    var assetManager = AssetManager.getInstance();


    assetManager.imgs = {
        "a": "game/data/images/a.png",
        "ground": "game/data/images/ground.png"
    };
    
    assetManager.finished = function() {
        console.log("load finish");
        initGame();
        run();
    };
    
    assetManager.downloadAll();


    var requestAnimFrame = (function() {
        return  window.requestAnimationFrame ||
                window.webkitRequestAnimationFrame ||
                window.mozRequestAnimationFrame ||
                window.oRequestAnimationFrame ||
                window.msRequestAnimationFrame ||
                function(callback, element) {
                    window.setTimeout(callback, 1000 / 60);
                };
    })();

    var x = 0;
    var y = 0;

    var mainScene = new Scene();
    sceneManager.add(mainScene);
    this.player = {};
    this.ground = {};

    function initGame() {
        player = new AnimateSprite(assetManager.imgs["a"], 0, 0, 64, 96);
        player.setPosition(10, 10);
        player.type = "player";
        player.collidableWith = "ground";
        mainScene.addSceneNode(player);

        ground = new Sprite(assetManager.imgs["ground"], 0, 0, 640, 32);
        ground.setPosition(0, 400);
        ground.type = "ground";
        mainScene.addSceneNode(ground);
    }

    function run() {
        requestAnimFrame(run);
        if (eventHandler.keyPress(37))
            player.move(-3, 0);
        if (eventHandler.keyPress(38))
            player.move(0, -3);
        if (eventHandler.keyPress(39))
            player.move(3, 0);
        if (eventHandler.keyPress(40))
            player.move(0, 3);
        sceneManager.update();
        x++;
        y++;

    }

    

})(); 